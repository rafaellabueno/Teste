package org.bigolin.bigoflix;

import java.util.ArrayList;
import org.bigolin.model.Filme;

public class BigoflixTeste {

    public static void main(String[] args) {
        //Lista preferidos = new Lista(10);
        ArrayList<Filme> preferidos = new ArrayList <Filme>();
        Filme x = new Filme();
        
        Filme f1 = new Filme();
        f1.setNome("O Senhor dos Anéis: O Retorno do Rei");//Método pertence a midia
        f1.setDiretor("Peter Jackson"); //Método pertence a filme
        f1.setDuracao(38000);
        preferidos.add(f1);
        System.out.println("Adiciona filme 1: "+preferidos);
        
        Filme f2 = new Filme();
        f2.setNome("As Tartarugas Ninja");
        f2.setDiretor("Jonathan Liebesman"); 
        f2.setDuracao(15000);
        preferidos.add(f2);
        System.out.println("Adiciona filme 2: "+preferidos);
        
        
        int index = preferidos.indexOf(f1);
        System.out.println("Retornando filme pelo index: "+preferidos.get(index));
        
        preferidos.remove(f1);
        System.out.println("Removeu filme 1: "+preferidos);
    }    
}
